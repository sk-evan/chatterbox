import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ChatService } from '../shared/chat.service';
import { WebsocketService } from '../shared/websocket.service';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
declare  var jQuery: any;

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom.component.html',
  styleUrls: ['./chatroom.component.css']
})


export class ChatroomComponent implements OnInit {

  @Output() messageToEmit = new EventEmitter<string>();
  constructor(
    public route: ActivatedRoute,
    public webSocketService: WebsocketService,
    public chatService: ChatService,
    public router: Router
  ) {
    this.webSocketService.newMessageReceived().subscribe(data => {

      this.messageArray.push(data);
      this.isTyping = false;
    });
    this.webSocketService.newChannelMessageReceived().subscribe(data => {

      this.channelmessageArray.push(data);
      this.isTyping = false;
    });
    this.webSocketService.receivedTyping().subscribe(bool => {
      this.isTyping = bool.isTyping;
    });
  }
  public username: any;
  public channelBox: any;
  public email: any;
  public chatroom;
  public message: any;
  public currentUser;
  messageArray ;
  channelmessageArray ;
  public isTyping = false;
  sk = false;
  check = false;
  flag = 0;
  chat: any = [];
  channelName: any;
  users = [];
  Users ;
  addUsers = [];
  selectedUsers: any[];
  deletedUsers: any[];
  admin: any;
  addnew: any;
  islogin: any = [];
  textarea;

  ngOnInit() {


 (function($) {
  $(document).ready(function() {

    const textarea = document.querySelector('textarea');
    textarea.addEventListener('keydown', autosize);
    function autosize() {
      const el = this;
      setTimeout(function() {
        el.style.cssText = 'padding:0;max-height:700px';
        // for box-sizing other than "content-box" use:
        el.style.cssText = '-moz-box-sizing:content-box';
        console.log(el.scrollHeight);
        if (el.scrollHeight < 200) {
        el.style.cssText = 'height:' + el.scrollHeight + 'px';
        }
        else{
           el.style.cssText = 'overflow:scroll';
          el.style.cssText = 'height: 200px';
        }

      }, 0);
    }
  });
})(jQuery);


 this.currentUser = this.chatService.getLoggedInUser();


 this.webSocketService.userJoinRoom()
    .subscribe(data => {
      this.channelmessageArray.push(data);
    });


 this.webSocketService.userLeftRoom()
    .subscribe(data => {

      this.channelmessageArray.push(data);
    });

 this.addnew = 0;
 this.chatService.getUsers().subscribe(users => {
      this.Users = users;


    });



 this.username = this.chatService.reLoad$.subscribe((user) => {
      this.flag = 0;
      this.sk = true;
      this.username = user;
      const currentUser = this.chatService.getLoggedInUser();

      if ( currentUser < this.username) {
      this.chatroom = currentUser.concat(this.username);
    } else {
      this.chatroom = this.username.concat(currentUser);
    }
      this.webSocketService.joinRoom({user: this.chatService.getLoggedInUser(), room: this.chatroom});
      this.chatService.getChatRoomsChat(this.chatroom).subscribe(messages => {
        this.chat = messages;
        if (this.chat.chatroom.length > 0) {
        this.messageArray = this.chat.chatroom[0].messages;
        }

    });
  });

 this.channelBox = this.chatService.reLoad.subscribe((name) => {

   this.flag = 1;
   this.sk = true;
   this.channelName = name;

   this.chatService.getChatChannelChat(this.channelName).subscribe(messages => {
      this.chat = messages;

      this.users = this.chat.chatroom[0].users;
      this.admin = this.chat.chatroom[0].admin;

      this.Users.forEach(element => {

        if (!this.users.find(user => user == element.username)) {
          this.addUsers.push(element.username);
        }

      });

      if (this.chat.chatroom.length > 0) {
      this.channelmessageArray = this.chat.chatroom[0].messages;
      }

  });
});

  }



  sendMessageToParent() {

    this.chatService.refresh2();
}

  change(event) {

    this.check = true;
  }
  sendMessage() {
    if (this.message !== undefined && this.message !== '') {
      const currentTime = moment().format('hh:mm a');
      this.webSocketService.sendMessage({room: this.chatroom, user: this.chatService.getLoggedInUser(),
      message: this.message, time: currentTime});
      this.message = '';
  }
  }

  sendChannelMessage() {
    if ( this.message !== undefined && this.message !== '') {
      const currentTime = moment().format('hh:mm a');
      this.webSocketService.sendChannelMessage({room: this.channelName, user: this.chatService.getLoggedInUser(),
       message: this.message, time: currentTime});
      this.message = '';
    }
  }

  typing() {
    this.webSocketService.typing({room: this.chatroom, user: this.chatService.getLoggedInUser()});
  }



   openNav() {
    document.getElementById('mySidebar').style.width = '350px';
    document.getElementById('main').style.marginRight = '350px';
    // document.getElementById('main').style.padding = '0px';
  }

   closeNav() {
    document.getElementById('mySidebar').style.width = '0';
    document.getElementById('main').style.marginRight = '0';
    // document.getElementById('main').style.padding = '10px';

  }


  fav() {
    const star = document.getElementById('favo');
    star.classList.toggle('fas');

  }

  addMembers() {
    this.addnew = 1;
  }

  deleteMembers() {
    this.addnew = 2;

  }

  addNewUsers(data: NgForm) {
    this.addnew = 0;

    const currentTime = moment().format('hh:mm a');


    this.webSocketService.updateChannel({
      room: this.channelName,
      users: this.selectedUsers,
      time: currentTime
    });

  }

  addDelete(data: NgForm) {


    this.addnew = 0;
    const currentTime = moment().format('hh:mm a');

    this.webSocketService.DeleteUsers({
      room: this.channelName,
      users: this.deletedUsers,
      time: currentTime
    });



  }











}
