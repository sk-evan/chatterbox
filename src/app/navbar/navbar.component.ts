import { Component, OnInit, OnDestroy, Output } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { ChatService } from '../shared/chat.service';
import { NgForm } from '@angular/forms';
import { format } from 'url';
import { WebsocketService } from '../shared/websocket.service';
import { element, EventEmitter } from 'protractor';

declare var jQuery: any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  public userIsAuthenticated = false;
  receivedChildMessage: boolean;

  private authListnerSubs: Subscription;
  message: string;
  name: string;
  public Users;
  public Checks;
  public sk: any = [];
  public rohanNoob = [];
  public value: string[];
  public Channels;
  public rooms = [];
  userFilter: any = { username: '' };
  roomFilter: any = [];
  room;
  selectedCity: any;
  selectedCityIds = [];
  selectedCityName = 'Vilnius';
  selectedCityId: number;
  selectedUsers: any[];
  checkboxes: any = [];
  names: any = [];
  chatroom: string;
  currentUser: string;

  constructor(
    public authService: AuthService,
    public chatService: ChatService,
    public webSocketService: WebsocketService
  ) {}
  selectedPeople = [];

  people$: Observable<any[]>;

  ngOnInit() {
    this.currentUser = this.chatService.getLoggedInUser();

    this.people$ = this.sk;


    this.getDataofUsers();

    this.chatService.reLoad2
    .subscribe(res => {
      const element = document.getElementById('sidebar');
      element.classList.toggle('active');

      const element1 = document.getElementById('content');
      element1.classList.toggle('active');

    });
    // (function($) {
    //   $(document).ready(function() {
    //     $('#sidebarCollapse').on('click', function() {
    //       $('#sidebar, #content').toggleClass('active');
    //       $('.collapse.in').toggleClass('in');
    //       $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    //     });
    //   });
    // })(jQuery);

    this.chatService.getChannels().subscribe(channels => {
      this.Channels = channels;
    });

    this.name = localStorage.getItem('username');
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListnerSubs = this.authService
      .getAuthStatusListner()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });
  }



  getUser() {
    return this.chatService.getLoggedInUser();
  }


  getDataofUsers() {
     this.chatService.getUsers().subscribe(users => {
      this.Users = users;

      this.getChannelData();

      this.Checks = users;

      let i = 0;

      this.Checks.forEach(element => {
        if (element.username == this.currentUser) {
          this.Checks.splice(i, 1);
        }
        i++;
      });

    });
  }

  sendData(data) {
    this.chatService.refresh(data);
  }

  sendChannelMessage(channelname) {
    this.chatService.refresh1(channelname);
  }
  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authListnerSubs.unsubscribe();
  }

  addUsers(data: NgForm) {
    if (data.invalid) {
      return true;
    }

    this.selectedUsers.push(this.currentUser);

    this.webSocketService.createChannel({
      admin: this.currentUser,
      room: data.value.channelname,
      users: this.selectedUsers
    });
    data.reset();
    jQuery('#exampleModal1').modal('hide');
  }


  getChannelData() {

     this.Users.forEach(element => {
        if (element.username === this.chatService.getLoggedInUser()) {
          for (let i = 0; i < element.room.length; i++) {
            this.rooms.push(element.room[i]);
          }
        }
      });
  }

  // checkbox(name, data1) {
  //   if (data1 === true) {
  //     this.names.push(name);
  //   } else {
  //     this.names.splice(this.names.indexOf(name), 1);
  //   }
  // }

  // this.Checks = users;

  //     let i = 0;

  //     console.log(this.Checks);
  // this.Checks.forEach(element => {
  //   if (element.username == this.currentUser) {
  //     this.Checks.splice(i,1);

  //   }
  //   i++;
  // });
}
